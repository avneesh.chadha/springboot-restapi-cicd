package com.bookshop.BookShop;

import com.bookshop.model.Book;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;


@RestController
public class BookController {


    @ResponseBody @RequestMapping("/Application")
    public Book index() {
        Book book = new Book("TestName", "TestAuthor");
        return book;
    }

}

