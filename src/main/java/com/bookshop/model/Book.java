package com.bookshop.model;

public class Book {

    private String name;
    private String author;



    public Book(String Name, String Author){

        this.name = Name;
        this.author = Author;
    }

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }
}
