package com.bookshop.BookShop;

import com.bookshop.model.Book;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.test.web.servlet.MockMvc;




public class BookControllerTest {

    private MockMvc mockMvc;



    @Test
    public void shouldReturnBookObject(){
        BookController bookController = new BookController();
        Book book = bookController.index();


        Assert.assertEquals(book.getName(), "TestName");
        Assert.assertEquals(book.getAuthor(), "TestAuthor");
    }
}
