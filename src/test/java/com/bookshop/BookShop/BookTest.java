package com.bookshop.BookShop;

import com.bookshop.model.Book;
import org.junit.Assert;
import org.junit.Test;
import org.junit.rules.TestName;

public class BookTest {

    @Test
    public void shouldReturnCorrectAuthorName(){
        Book book = new Book("TestName", "TestAuthor");
        Assert.assertEquals("TestName", book.getName());
    }


    @Test
    public void shouldReturnCorrectTitleName(){
        Book book = new Book("TestName", "TestAuthor");
        Assert.assertEquals("TestAuthor", book.getAuthor());

    }
}
